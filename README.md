# Cursed's harcore mode

> ## Goal
> 
> This mod aims to make minecraft harder, you will start the game with only 3 hearts but by defeating some mobs, boss or even by trading you will be able to gain more heart

## Features

### Items

- Absorbtion hearts : a new item that will give you absorption hearts
- Hearts : with this item you can obtain more permanent heart

### Other features

- When you die you will lose one permanent heart
- Killing mobs can sometimes gives you absorption hearts or permanent hearts

## Versions

It currently supports minecraft 1.18.1 with Forge. 
**I do not recommend you to use version of my mod before Cursed's harcore mode - 1.14 - 1.18.1 since you will have issue on server and compatibility with other mods**
- I wont backport the mod

- But I will probably port it to future versions

## Configuration file

There is a configuration file, it is named "config-cursedshardcoremode.toml". You can find it under .minecraft/config

## Links

> You can download the mod on curseforge : https://www.curseforge.com/minecraft/mc-mods/curseds-hardcore-mode
